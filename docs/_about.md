### **Web Shop**

- Web Shop (WP) là trang thanh toán games của VNG, phục vụ local user thanh toán với các phương thức thanh toán do GT cung cấp

- Web Shop triển khai ở thị trường Việt Nam và SEA:
    - Việt Nam: [https://pay.zing.vn](https://pay.zing.vn)
    - SEA: [https://shop.vng.games](https://shop.vng.games)

### **Chức năng**

**1. Front-end cho người dùng**

- Mainsite hiển thị danh sách games của VNG. Thứ tự hiển thị game dựa theo lịch sử truy cập của User
- Đăng nhập thông qua APIs [GT Login Gateway](/GTPassport/logingw)
- Chọn server, roles, vật phẩm, thanh toán sử dụng [GT Billing](/GTBilling/_about)

**2. Ghi nhận và phân tích hành vi User**

- Tích hợp với Google Analytics (GA), Google Tag Manager (GTM), Facebook Pixcel, Firebase, Appsflyer, Hot jar ...

**3. Cung cấp và hỗ trợ Promotion**

- Hiển thị các chương trình promotion của GT, ZaloPay:
    - Gói quà tặng: User truy cập Web Shop để nhận quà tặng (theo điều kiện)
    - Promotion Signal: Hiển thị các chương trình promotion mà User phù hợp 
    - Thanh toán nhận quà tặng: Khi User mua thành công vật phẩm sẽ được tặng quà hoặc code 
    - ZaloPay Cashback: Khi User mua vật phẩm bằng ZaloPay sẽ được hoàn tiền vào ví ZaloPay
    - ...

**4. Cung cấp APIs mở Web Shop từ Client**

- Cung cấp APIs tạo link thanh toán và kiểm tra thanh toán cho VNG SDK

### **Tài liệu tích hợp**

- [New theme](/GTWebShop/newtheme)
- [Tracking](/GTWebShop/tracking)
- [Web Shop Integration](/GTWebShop/integration)



