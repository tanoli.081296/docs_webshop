_|_|
-------|-|
Version| 1.0
Release date| 09-12-2019
Description| Tài liệu tích hợp với Web Shop

**Change log**

Version|Description|
-|-|
1.0|Khởi tạo



**List APIs**

_|Name|Description
-|----|-----------
1|[API getWebInfo](#1-api-getwebinfo) | API cung cấp thông tin về Web view
2|[API getOrderStatus](#2-api-getorderstatus) | API cung cấp thông tin đơn hàng

<div class="pagebreak"></div>

### 1. API getWebInfo

* Sandbox Url: https://sandbox-pay.mto.zing.vn/lp/shopfront/getWebInfo
* Production Url: https://pay.mto.zing.vn/lp/shopfront/getWebInfo
* Protocol: HTTP POST/GET
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    shopfrontID `required`|String|Tên shopfront
    userID `required`|String|userID
    loginType `optional`|String|Dành cho game có sử dụng loginChannel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    serverID `optional` |String|Dành cho game có sử dụng serverID
    roleID `optional`|String|Dành cho game có sử dụng roleID
    roleName `optional`|String|Dùng để hiển thị trên Shopfront
    serverName `optional`|String|Dùng để hiển thị trên Shopfront
    productID`optional`|String|Chọn sẵn vật phẩm
    lang `required`|String|ISO 639-1 codes. E.g vi,en,…Referring to [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
    country `optional`|String|ISO 3166-1 alpha-2 codes. E.g VN,TH,IN,…Referring to [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
    currency `optional`|String|ISO 4217. E.g VND,THB,USD,… Referring to [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    ts `required`|Long|timestamp in milliseconds
    sig `required`|String|Referring to signature generation in [Appendix 1](#appendix-2-create-signature) 

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|ShopfrontInfoEntity|Thông tin shopfront

* ShopfrontInfoEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    shopfrontUrl|String|Url mở ra shopfront
    checkOrderUrl|String|Url dùng để check status
    jtoken|String|token sử dụng để check status
    clientKey|String|key sử dụng để check status

<div class="pagebreak"></div>

### 2. API getOrderStatus

* Production Url: https://billing.mto.zing.vn/fe/api/order/getStatus
* Sandbox Url: https://sandbox-billing.mto.zing.vn/fe/api/order/getStatus
* Protocol: HTTP GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientKey `required`|String|Client Key (lấy từ kết quả của [API getPageInfo](#1-api-getpageinfo))
    orderNumber `required`|String| Mã đơn hàng (Lấy từ URL của Shopfront. Ex: https://pay.zing.vn/product/thiennu/result/1529291148122660864)
    jtoken `required`|String|token (lấy từ kết quả của [API getPageInfo](#1-api-getpageinfo))
    lang `required`|String|ISO 639-1 codes. E.g vi,en,…Referring to [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
    country `optional`|String|ISO 3166-1 alpha-2 codes. E.g VN,TH,IN,…Referring to [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|OrderStatusEntity|Thông tin đơn hàng

* OrderStatusEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    orderStatus|Integer|status code <br/>`success` = 1 <br/> `fail` != 1
    orderStatusMessage|String|status message


<div class="pagebreak"></div>

##Appendix 1. Login channel
 
Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple

##Appendix 2. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:

    ```bash
    URL: "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    Secret key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "game" + "item" + "1" + "6789" + "test" + "11517900546676" + "12345"
    Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcgameitem16789test1151790054667612345") = "6d1ecb0d9e9d118e032f9c8b5f0ad866"
    ```
