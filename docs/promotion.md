### **Web Shop Promotion**

- Web Shop Promotion là các chương trình promotion hiển thị trên Web Shop.

### **Các chương trình Promotion**

**1. Gói quà tặng**
- Gói quà tặng: User truy cập Web Shop để nhận quà tặng (theo điều kiện)

**2. Promotion Signal**
- Hiển thị các chương trình promotion mà User phù hợp 

**3. Thanh toán nhận quà tặng**
- Tặng quà hoặc code khi User mua vật phẩm thành công 

**4. ZaloPay Cashback**
- Hoàn tiền vào ví ZaloPay khi mua vật phẩm
