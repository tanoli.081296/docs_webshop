# HƯỚNG DẪN KIỂM TRA VÀ NGHIỆM THU WEBSHOP VIỆT NAM


![](flow.drawio)

Sơ đồ hướng dẫn các bước kiểm tra tính năng và hiển thị WebShop Việt Nam

        
[**1. Kiểm tra Danh sách Cửa Hàng**](#1-kiem-tra-danh-sach-cua-hang)
        
[**2. Kiểm tra Đăng nhập và thông tin Nhân Vật**](#2-kiem-tra-ang-nhap-va-thong-tin-nhan-vat)
        
[**3. Kiểm tra Danh sách vật phẩm**](#3-kiem-tra-danh-sach-vat-pham)
        
[**4. Kiểm tra Thanh toán đơn hàng**](#4-kiem-tra-thanh-toan-on-hang)
    
[**5. Kiểm tra Kết quả đơn hàng**](#5-kiem-tra-ket-qua-on-hang)

## 1. Kiểm tra Danh sách Cửa hàng 
    
- *Truy cập <https://pay.zing.vn> trên <span style="color: red;font-weight: bold;">PC/Laptop và Mobile/Tablet</span> kiểm tra theo checklist:*
        
    - Hình ảnh (banner) đã hiển thị trên danh sách Cửa hàng 
        
    - Hình ảnh Cửa hàng đúng với hình khai báo và thiết kế
        
    - Tên Game đúng với tên đăng kí trên giấy phép <quy trình release shopfront>
        
    - Game được phân loại Game Mobile/Game PC chính xác <p class="red"><i>[Click vào để xem hình minh họa](#1)</i></p> 
        
    - Tên Game hiển thị ở *Thanh tìm kiếm* với tên đăng kí trên giấy phép <p class="red"><i>[Click vào để xem hình minh họa](#2)</i></p>
        
    - Bấm vào Cửa hàng trên Danh sách cửa hàng sẽ trả về  đúng *Trang đăng nhập* của Cửa hàng đã chọn
    
## 2. Kiểm tra đăng nhập và thông tin Nhân vật
    
- *Kiểm tra sản phẩm muốn kiểm tra trên <span style="color: red;font-weight: bold;">PC/Laptop và Mobile/Tablet</span> theo checklist:*

    - Có đầy đủ với các phương thức đăng nhập theo đúng yêu cầu <p class="red"><i>[Click vào để xem hình minh họa](#3)</i></p>
        
    - HÌnh ảnh banner, favicon, logo ở trang đăng nhập đúng theo yêu cầu trên PC/Laptop và Mobile/Tablet

    - Kiểm tra khi đăng nhập qua các kênh trung gian như Facebook, Zalo,.... thành công và trả  về đúng thông tin nhân vật in-game

    - Khi đăng nhập bằng ID nhân vât phải có hướng dẫn lấy ID Nhân vật trong in-game <p class="red"><i>[Click vào để xem hình minh họa](#4)</i></p>
       
    - Kiểm tra tài khoản trả về đúng cụm server, role, level theo đúng thông tin in-game <p class="red"><i>[Click vào để xem hình minh họa](#5)</i></p>
            
    - Những server bảo trì có hiện trong danh sách server/role hay không?
            
    - Thời gian delay khi thông tin nhân vật in-game thay đổi thì trên webshop thay đổi (tạo nv mới, tăng cấp nhân vật, merge server..)
        
    - Có mục lưu ý về sử dụng point khi đăng nhập bằng ID Nhân vật <p class="red"><i>[Click vào để xem hình minh họa](#6)</i></p>
        
    - Khi đăng nhập bằng ID nhân vật trả đúng về thông tin nhân vật in-game

## 3. Kiểm tra Danh sách vật phẩm

- *Kiểm tra hình ảnh các gói vật phẩm đúng yêu cầu trên các loại thiết bị <span style="color: red;font-weight: bold;">PC/Laptop và Mobile/Tablet</span> theo checklist:*

    - Kiểm tra các gói vật phẩm có điều kiện phải hiển thị đúng với yêu cầu in-game (chỉ bán trên server nào, yêu cầu lv nhân vật,...)

    - Kiểm tra các gói vật phẩm đầy đủ các phương thức thanh toán theo thông tin sản phẩm đăng kí <p class="red"><i>[Click vào để xem hình minh họa](#7)</i></p>

    - Gói vật phẩm đúng với tab hiển thị <p class="red"><i>[Click vào để xem hình minh họa](#8)</i></p>

    - Gói vật phẩm đúng với mô tả đã đăng kí và đúng với thông tin in-game <p class="red"><i>[Click vào để xem hình minh họa](#9)</i></p>

    - Gói vật phẩm được mua đúng với Tài khoản đã đăng nhập <p class="red"><i>[Click vào để xem hình minh họa](#10)</i></p>
    
- ***(Tùy chọn) Khuyến mãi tại trang danh sách vật phẩm:***
        
    - Promotion signal nằm trên header của page <p class="red"><i>[Click vào để xem hình minh họa](#11)</i></p>

    - Promotion signal hiện đúng thông tin đã đăng ký <p class="red"><i>[Click vào để xem hình minh họa](#11)</i></p>

    - Các khuyến mãi hiển thị theo điều kiện đã đăng ký <p class="red"><i>[Click vào để xem hình minh họa](#11)</i></p>

    - Các gói vật phẩm có *chương trình khuyến mãi* sẽ được gắn theo đúng thông tin nhận diện <p class="red"><i>[Click vào để xem hình minh họa](#15)</i></p>

    - Hiển thị đúng thông tin đã đăng kí khi người dùng thao tác theo điều kiện nhận quà

    - Mục *Lịch sử nhận quà* trả về đúng danh sách *quà* đã nhận <p class="red"><i>[Click vào để xem hình minh họa](#14)</i></p>

## 4. Kiểm tra Thanh toán đơn hàng

- *Hướng dẫn kiểm tra các phương thức thanh toán trên môi trường Sandbox*

    - Thanh toán Zing Card <p class="red"><i>[Click vào để xem hướng dẫn chi tiết](../../../docs_gtqc/Zingcard)</i></p>

    - Thanh toán SMS <p class="red"><i>[Click vào để xem hướng dẫn chi tiết](../../../docs_gtqc/SMS)</i></p>

    - Thanh toán Zalopay <p class="red"><i>[Click vào để xem hướng dẫn chi tiết](../../../docs_gtqc/Zalo Pay)</i></p>

    - Thanh toán Thẻ tín dụng <p class="red"><i>[Click vào để xem hướng dẫn chi tiết](../../../docs_gtqc/Thẻ tín dụng)</i></p>

    -  Thanh toán ATM/iBanking*

        - Test trên Sandbox: **chỉ test được flow tới bước hiển thị form nhập thông tin.**

        - Test trên Production: Dùng tài khoản hoặc thẻ ATM có iBanking để thanh toán

- *Khi sử dụng các phương thức thanh toán trên ở môi trường real sẽ thanh toán bằng tiền thật*

- *Kiểm tra các phương thức thanh toán trên các loại thiết bị <span style="color: red;font-weight: bold;">PC/Laptop và Mobile/Tablet</span> theo checklist:*

    - Flow của tất cả các kênh thanh toán đúng theo đăng kí

    - Có message đúng cho các trường hợp nạp thiếu, nạp dư cho game có sử dụng point 

    - Kiểm tra đúng và đủ message cho trường hợp Game có filter mệnh giá nạp Zing Card <p class="red"><i>[Click vào để xem hình minh họa](#12)</i></p>
        
## 5. Kiểm tra Kết quả đơn hàng

- *Kiểm tra kết quả đơn hàng đúng yêu cầu các loại thiết bị <span style="color: red;font-weight: bold;">PC/Laptop và Mobile/Tablet</span>    theo checklist:*
        
    - Kết quả giao hàng thành công

    - Kiểm tra kết quả giao dịch và trang kết quả giao dịch hiện đúng Message

    - Sau khi *Kiểm tra kết quả giao dịch* redirect về đúng trang *Danh sách vật phẩm*
        
    - Giao hàng trong in-game đúng vật phẩm user đã mua và giống như trên WebShop.
    
- ***(Tùy chọn) Khuyễn mãi tại trang Kết quả đơn hàng:***
        
    - Promotion signal tại trang *Kết quả giao dịch* hiện đúng theo đăng kí <p class="red"><i>[Click vào để xem hình minh họa](#13)</i></p>

    - Promotion giftcode thì các giftcode phải hợp lệ <p class="red"><i>[Click vào để xem hình minh họa](#13)</i></p>

    - Promotion redirect link thì redirect đúng theo yêu cầu

# **Hình ảnh minh họa Nghiệm thu**

 <a name="1"></a> 

## - Hình 1: Hình minh họa 2 tab Game PC và Game Mobile trên WebStore 

![](media/1.tab.png) 

<a name="2"></a>

## - Hình 2: Hình minh họa Game: *SamruraiShodown* trên thanh tìm kiếm theo như thông tin đã đăng ký

![](media/1.tenhienthi.png) 

<a name="3"></a>

## - Hình 3: Hình minh họa các *Phương thức đăng nhập* hiện có của Game *SamruraiShodown* 

![](media/2.ptdn.png) 

<a name="4"></a>

## - Hình 4: Hình minh họa *Hướng dẫn lấy ID nhân vật in-gamne* 

![](media/2.snk.png) 

 <a name="5"></a>

## - Hình 5: Hình minh họa giao diện *Danh sách server, role, level,...*

![](media/2.server.png) 

<a name="6"></a>

## - Hình 6: Hình minh họa Lưu ý về việc sử dụng point khi đăng nhập bằng *ID nhân vật* 

![](media/jxm1.png) 

<a name="7"></a>

## - Hình 7: Hình minh họa các phương thức thanh toán tại trang *danh sách vật phảm* 

![](media/3.sms.png) 

<a name="8"></a>

## - Hình 8: Hình minh họa *tab(group)* của các gói vật phẩm 

![](media/3.tab.png) 

<a name="9"></a>

## - Hình 9: Hình minh họa mục *Mô tả* của gói vật phẩm 

![](media/3.mota.png) 

<a name="10"></a>

## - Hình 10: Hình minh họa *thông tin tài khoản* sẽ nhận được vật phẩm sau khi thanh toán thành công 

![](media/3.thongtin.png) 

<a name="11"></a>

## - Hình 11: Hình minh họa *Khuyến mãi tại trang danh sách vật phẩm* 

![](media/promotion.png) 

<a name="15"></a>

## - Hình 12: Hình minh họa *Tag của các gói vật phẩm khuyến mãi tại trang danh sách vật phẩm*

![](media/3.tag.p.png) 

<a name="14"></a>

## - Hình 13: Hình minh họa *Lịch sử nhận quà tại trang danh sách vật phẩm* 

![](media/lichsu.png) 

<a name="12"></a>

## - Hình 14: Hình minh họa trang thanh toán bằng *Zing Card* có tính năng lọc mệnh giá 

![](media/filter.png) 

<a name="13"></a>

## - Hình 15: Hình minh họa *Khuyến mãi tại trang kết quá thanh toán* 

![](media/promotionsignal.png) 


<style>
.red a{
  color:red
}
</style>