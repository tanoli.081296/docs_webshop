<style>
.md-typeset table:not([class]) td {
  word-break: break-word;
  padding: 5px;
  border: 0.05rem solid rgba(0, 0, 0, 0.07);
}

.md-typeset table:not([class]) td:first-child {
  font-weight: 700;
  font-size: 10px;
  /* text-align: center; */
  vertical-align: middle;
  min-width: 150px;
}

.md-typeset table:not([class]) th:nth-child(1){
  min-width: 200px;
}

.md-typeset table:not([class]) th:nth-child(2){
  min-width: 40px;
}
</style>

# Tracking hành vi người dùng trên Web Shop

## 1. Mô tả dịch vụ

GT Tracking là hệ thống thu thập hành vi người dùng khi thực hiện các bước thanh toán trên Web Shop. Nhằm cung cấp các thông tin để phân tích, hiểu được người dùng thường mua gói vật phẩm, thực hiện các thao tác gì trên trang thanh toán thông qua Google Analytics. Ngoài ra hệ thống có thể gửi các thông tin này tới các hệ thống Google Firebase phục vụ mục đích mục đích quảng bá của sản phẩm.

## 2. Nền tảng sử dụng

GT Tracking hoạt động dựa trên Google Analytics platform để thu thập thông tin hành vi người dùng trên Web Shop. Ngoài ra còn sử dụng Google Firebase để có thể đồng bộ dữ liệu trong ứng dụng. Bên cạnh đó, GT Tracking còn hỗ trợ về Facebook Pixel, Google Adwords. Ngoài ra còn hỗ trợ gắn thêm Google Adword, Google Tag Manager

## 3. Thông tin tracking
<table style="width:100%">
<thead>
  <tr>
    <th>Events</th>
    <th>Nền tảng</th>
    <th>Đường dẫn</th>
    <th>Mô tả</th>
    <th>Dữ liệu thu thập</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>webpayment_pageview</td>
    <td>GF</td>
    <td rowspan="2">/identification /package /payment /confirmation /result</td>
    <td rowspan="2">Event sẽ phát sinh ở các bước tương ứng với location</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>pageview</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_display</td>
    <td>GF</td>
    <td rowspan="2">/identification</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có chọn thông tin server/role, khi user truy cập đến location</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, isSuggestionRole, isSuggestedServer, serverType, roleType</td>
  </tr>
  <tr>
    <td>display</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_input_server</td>
    <td>GF</td>
    <td rowspan="2">/identification</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có chọn thông tin server</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>input_server</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_input_role</td>
    <td>GF</td>
    <td rowspan="2">/identification</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có chọn thông tin role</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>input_role</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_identification</td>
    <td>GF</td>
    <td rowspan="2">/identification</td>
    <td rowspan="2">Event sẽ phát sinh nếu user bấm nút "Chọn nhân vật này": thông tin về server, role mà người dùng đã chọn</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, isSuggestionRole, isSuggestedServer, status</td>
  </tr>
  <tr>
    <td>submit_identification</td>
    <td>GA, FP</td>
  </tr>
  <tr>
    <td>webpayment_display_product_suggestion</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có gói vật phẩm suggest</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, sOrder</td>
  </tr>
  <tr>
    <td>display_product_suggestion</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_select_product</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có chọn gói vật phẩm</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, gOrder, pOrder</td>
  </tr>
  <tr>
    <td>select_product</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_select_product_promotion</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có gói vật phẩm promotion và user thực hiện thao tác chọn gói đó</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, gOrder, pOrder</td>
  </tr>
  <tr>
    <td>select_product_promotion</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_select_product_suggestion</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có gói vật phẩm suggest</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, sOrder</td>
  </tr>
  <tr>
    <td>select_product_suggestion</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_product</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng bấm submit gói vật phẩm</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, sOrder, gOrder, pOrder</td>
  </tr>
  <tr>
    <td>submit_product</td>
    <td>GA, FP</td>
  </tr>
  <tr>
    <td>webpayment_submit_product_promotion</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng bấm submit gói vật phẩm promotion</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, sOrder, gOrder, pOrder</td>
  </tr>
  <tr>
    <td>submit_product_promotion</td>
    <td>GA, FP</td>
  </tr>
  <tr>
    <td>webpayment_display_promotion_signal</td>
    <td>GF</td>
    <td rowspan="2">/package</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có thông tin promotion singal</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, psOrder</td>
  </tr>
  <tr>
    <td>display_promotion_signal</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_select_payment_method</td>
    <td>GF</td>
    <td rowspan="2">/payment</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng chọn phương thức thanh toán</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, paymentMethod</td>
  </tr>
  <tr>
    <td>select_payment_method</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_payment</td>
    <td>GF</td>
    <td rowspan="2">/payment</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng bấm submit</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, paymentCountryID, paymentCurrencyID, paymentGatewayID, paymentMethodID, paymentPartnerID, paymentProviderID, paymentProviderName</td>
  </tr>
  <tr>
    <td>submit_payment</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_display_confirmation</td>
    <td>GF</td>
    <td rowspan="2">/confirmation</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có hiển thị trang confirmation</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>display_confirmation</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_order</td>
    <td>GF</td>
    <td rowspan="2">/confirmation</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpayment có hiển thị trang confirmation và người dùng bấm nạp tiền</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, orderNumber, gOrder, pOrder, sOrder</td>
  </tr>
  <tr>
    <td>submit_order</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_next</td>
    <td>GF</td>
    <td rowspan="2">/confirmation</td>
    <td rowspan="2">Event sẽ phát sinh nếu thực hiện tạo thông tin giao dịch thành công</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>submit_next</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_submit_check_suggestion_order</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu thực hiện thao tác Kiểm tra kết quả giao dịch với gói suggest</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>submit_check_suggestion_order</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>weybpayment_submit_check_order_status</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu thực hiện thao tác kiểm tra kết quả giao dịch</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, productId, status</td>
  </tr>
  <tr>
    <td>submit_check_order_status</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_back_to_products</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu Người dùng bấm nút "Tiếp tục nạp nhân vật này"</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>back_to_products</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_back_to_identification</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng bấm nút "Tiếp tục nạp nhân vật khác"</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID</td>
  </tr>
  <tr>
    <td>back_to_identification</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_select_product_suggestion</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu người dùng chọn gói được suggest</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, sOrder</td>
  </tr>
  <tr>
    <td>select_product_suggestion</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_display_promotion_signal_monthly</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpay có thông tin promotion singal monthly</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, psOrder</td>
  </tr>
  <tr>
    <td>display_promotion_signal_monthly</td>
    <td>GA</td>
  </tr>
  <tr>
    <td>webpayment_display_promotion_signal_accumulation</td>
    <td>GF</td>
    <td rowspan="2">/result</td>
    <td rowspan="2">Event sẽ phát sinh nếu webpay có thông tin promotion singal accumulation</td>
    <td rowspan="2">screenHeight, screenWidth, platform, OS, shopfrontID, pagePath, category, userID, psOrder</td>
  </tr>
  <tr>
    <td>display_promotion_signal_accumulation</td>
    <td>GA</td>
  </tr>
</tbody>
</table>
-------------------------------------------
- **GA**: Google analytics
- **GF**: Google firebase
- **FP**: Facebook pixel
- **orderNumber**: Mã giao dịch.
- **productId**: ID vật phẩm.
- **gOrder**: Vị trí nhóm gói vật phẩm trên webpayment.
- **pOrder**: Vị trí gói vật phẩm trên webpayment.
- **sOrder**: Vị trí gói vật phẩm được gợi ý trên webpayment.
- **isSuggestionRole**: Sản phẩm có hỗ trợ gợi ý Role hay không.
- **isSuggestedServer**: Sản phẩm có hỗ trợ gợi ý Server hay không.

## 4. Hướng dẫn cấu hình Tracking

**Vậy phía Product cần phải làm gì để có thể lấy được các thông tin hành vi người dùng trên Webpayment?**

=> Product cần cung cấp các thông tin như GaID, FacebookPixelID ... hoặc thông tin cấu hình Firebase cho GT cấu hình.

- [Hướng dẫn lấy thông tin Google Analytics](../tracking/tracking_ga)
- [Hướng dẫn lấy thông tin Google Tag Manager](../tracking/tracking_ggtm)
- [Hướng dẫn lấy thông tin Facebook Pixel](../tracking/tracking_facebook)
- [Hướng dẫn lấy thông tin Google Firebase](../tracking/tracking_firebase)