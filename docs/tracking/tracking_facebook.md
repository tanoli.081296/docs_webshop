# Hướng dẫn lấy thông tin Facebook Pixel

**Bước 1**: Đầu tiên bạn đăng nhập vào trang [Facebook Apps](https://developers.facebook.com/apps/).

![Facebook Pixel](./static/fbp-step-1.png)

**Bước 2**: Lấy ID Ứng dụng của bạn email tới gt.framework để cấu hình tracking
