# Hướng dẫn lấy thông tin Google Firebase

**Bước 1**: Đầu tiên bạn đăng nhập vào trang [Google Firebase](https://console.firebase.google.com). Nếu chưa có dự án, bạn phải khởi tạo 1 dự án mới.

![Google Firebase](./static/ggfb-step-1.png)

**Bước 2**: Sau khi khởi tạo dự án. Bạn cần **Thêm ứng dụng cho dự án**. Bạn chọn hình tròn có biểu tượng **</>**.

![Google Firebase](./static/ggfb-step-2.png)

**Bước 3**: Sau khi thêm ứng dụng xong. Bạn bấm vào ứng dụng vừa thêm để xem thông tin ứng dụng.

![Google Firebase](./static/ggfb-step-3.png)

**Bước 4**: Tại đây bạn chú ý tới object **firebaseConfig**.

![Google Firebase](./static/ggfb-step-4.png)

**Bước 5**: Sau đó bạn email **firebaseConfig** tới gt.framework để được hỗ trợ cấu hình tracking.
