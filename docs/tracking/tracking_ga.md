# Hướng dẫn lấy thông tin Google Analytics

**Bước 1**: Đầu tiên bạn đăng nhập vào trang [Google Analytics](https://analytics.google.com/analytics/web). Sau đó bấm vào mục **quản trị**.

![Google Analytics](./static/ga-step-1.png)

**Bước 2**: Bấm vào tạo thuộc tính

![Google Analytics](./static/ga-step-2.png)

Tại đây có 2 lựa chọn cho bạn.

- [Lựa chọn đo lường các chỉ số của trang web](#huong-dan-tao-o-luong-cac-chi-so-cua-trang-web)
  ![Google Analytics](./static/ga-step-3.1.png)

- [Lựa chọn đo lường các chỉ số trên ứng dụng và web](#huong-dan-tao-o-luong-cac-chi-so-tren-ung-dung-va-web)
  ![Google Analytics](./static/ga-step-3.2.png)

## Hướng dẫn tạo Đo lường các chỉ số của trang web

**Bước 3**: Điền các thông tin cần thiết vào các ô nhập thông tin. Sau đó bấm nút **Tạo**

![Google Analytics](./static/ga-step-3.1.1.png)

**Bước 4**: Sau khi Tạo xong. Bấm vào _Cài đặt thuộc tính_ và tích + bật các mục 2,3,4

![Google Analytics](./static/ga-step-3.1.2.png)

**Bước 5**: Tiếp theo bấm vào _Thông tin theo dõi_ và chọn _Thu thập dữ liệu_. Tại đây bạn bật tính năng "Thu thập dữ liệu cho Google tín hiệu".

![Google Analytics](./static/ga-step-3.1.3.png)

**Bước 6**: Sau khi hoàn tất các bước trên. Vui lòng email **Id theo dõi** tới gt.framework để được cấu hình tracking.

---

## Hướng dẫn tạo Đo lường các chỉ số trên ứng dụng và web

**Bước 3**: Điền các thông tin cần thiết vào các ô nhập thông tin. Sau đó bấm nút **Tạo**

![Google Analytics](./static/ga-step-3.2.1.png)

**Bước 4**: Tiếp theo, chọn _Luồng dữ liệu_ và chọn platform là **WEB**

![Google Analytics](./static/ga-step-3.2.2.png)

**Bước 5**: Sau đó, bạn điền thông tin cấn thiết và bấm vào **Tạo luồng**

![Google Analytics](./static/ga-step-3.2.3.png)

Sau khi tạo xong, bạn sẽ thấy xuất hiện luồng bạn vừa tạo với các thông tin bạn đã điền. Tiếp theo bấm vào xem các thuộc tính của luồng vừa tạo.

![Google Analytics](./static/ga-step-3.2.4.png)

**Bước 6**: Tại màn hình này. Bạn cần sao chép **Mã đo lường**. Sau đó bấm vào _Thẻ trang web đã kết nối_.

![Google Analytics](./static/ga-step-3.2.5.png)

**Bước 7**: Dán **Mã đo lường** vào ô _Nhập mã thẻ để kết nối_ rồi bấm nút **Kết nối** để hoàn thành.

![Google Analytics](./static/ga-step-3.2.6.png)

**Bước 8**: Ngoài ra, bạn bấm vào mục _Cài đặt dữ liệu_, rồi chọn mục _Thu thập dữ liệu_.

Tại màn hình này, bạn bấm nút **Bắt đầu** và hoàn thành các bước hướng dẫn.

![Google Analytics](./static/ga-step-3.2.7.png)

**Bước 9**: Sau khi hoàn tất các bước trên. Vui lòng email **Mã đo lường** tới gt.framework để được cấu hình tracking.
