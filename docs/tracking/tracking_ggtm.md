# Hướng dẫn lấy thông tin Google Tag Manager

**Bước 1**: Đầu tiên bạn đăng nhập vào trang [Google Tag Manager](https://tagmanager.google.com).
Nếu chưa có tài khoản. Bạn bấm nút **Tạo tài khoản**.

![Google Tag Manager](./static/ggtm-step-1.png)

**Bước 2**: Nhập các thông tin cần thiết và sau đó bấm nút **Tạo**.

![Google Tag Manager](./static/ggtm-step-2.png)

**Bước 3**: Bạn chú ý tới ID được khoanh đỏ. Đó chính là ID định danh Google Tag Manager.
Tiếp theo, bạn bấm vào **thêm thẻ mới**.

![Google Tag Manager](./static/ggtm-step-3.png)

**Bước 4**: Bạn lựa chọn loại thể muốn liên kết với Google Tag Manager. Sau đó bấm nút **Gửi** (bên cạnh nút **Xem trước**) để hoàn thành việc tạo Google Tag Manager và gắn thẻ.

![Google Tag Manager](./static/ggtm-step-4.png)

**Bước 5**: Sau khi hoàn tất các bước trên. Vui lòng email **ID Google Tag Manager** tới gt.framework để được cấu hình tracking.
