# HƯỚNG DẪN DỬ DỤNG WEBSHOP OPERATION TOOL

## Giới thiệu chung

***WEBSHOP OPERATION TOOL là công cụ phục vụ cho việc quản lý cửa hàng (WebShop)***

***Chức năng công cụ ở phiên bản hiện tại gồm có:***

  - Bảo trì cửa hàng

  - Quản lý kho hàng (cấu hình nhóm và vật phẩm)

  - Xem thông tin về các kênh thanh toán của cửa hàng

  - Theo dõi lịch sử thao tác và phiên bản của cửa hàng 

  - Mỗi Game sẽ có thể được khởi tạo 2 WebShop: WebShop Sandbox và WebShop Live

  - Áp dụng (release) cập nhật cấu hình cửa hàng (chỉ áp dụng cho WebShop Live) 

## Tính năng

### I. Bảo trì cửa hàng

  - Bước 1: Chọn **Bật bảo trì** tại trang **Thông tin chung** của WebShop ở môi trường **LIVE**

  - Bước 2: Chọn thời gian **bắt đầu** và **kết thúc** của thời gian bảo trì

  - Bước 3: Điền thông tin hiển thị cho người dùng truy cập trong thời gian bảo trì

  ![](media/baotri2.png)

### II. Quản lý kho hàng

#### 1. Cấu hình **Nhóm**

  - Bươc 1: Chọn tab **Nhóm** của mỗi WebShop trên thanh Danh Mục để xem thông tin và trạng thái của **Nhóm**
    
  - Bước 2: Vào tab **Nhóm** và Chọn **Thêm nhóm mới**
    
  - Bước 3: Điền đúng tên Nhóm và chọn **Xác nhận”**
    
    - Có thể sửa tên Nhóm bằng cách chọn “Sửa” và thay đổi tên Nhóm

 ![](media/nhom.png)

#### 2. Cấu hình **Gói được bán**

 ![](media/hgoiduocban.png)

1. **Thông tin cấu hình** 

![](media/thongtincauhinh.png)

- In-game product ID (ID vật phẩm): ID vật phẩm

-  Loại gói:

    - Gói (Package): 1 vật phẩm hoặc một gói bao gồm nhiều vật phẩm (vd: túi phi thăng cánh, túi bổ sung nguyên tố,…)
 
    - Convert (chuyển đổi): chuyển đổi trực tiếp giá trị thanh toán của người chơi thành đơn vị tiền tệ in-game

- Trạng thái:  Trạng thái của gói vật phẩm

- Kiểu bán: có 3 dạng kiểu bán là Tiền và Point và cả 2

    - Tiền: Mua được trực tiếp bằng tiền qua các phương thức thanh toán mà gói vật phẩm hiện có

    - Point: Mua được bằng Point tồn của Người dùng hoặc Mua thêm số lượng Point cần mua.

    - Cả 2: Mua được gói vật phẩm bằng cả 2 kiểu bán trên

- Kiểu giá trị thanh toán

    -  Static: Giá trị vật phẩm được cố định, người dùng không thể thay đổi giá tiền thanh toán12000 VND

    - Dynamic: Giá trị vật phẩm thay đổi theo Người dùng  (Thường áp dụng với Loại gói Convert)

    - Static Denom: Bắt buộc thanh toán đúng với mệnh giá của Vật phẩm (vd: gói vật phẩm 50K chỉ có thể thanh toán được bằng card zing 50K, khi nhập 
    card zing 20K báo lỗi)

- Thứ tự hiển thị: Điều chỉnh thứ tự hiển thị của gói vật phẩm trên WebShop (số thấp sẽ hiển thị lên trên)

- Thời gian bán: có 2 tùy chọn thời gian bán của Gói vật phẩm

    - Không giới hạn thời gian: Áp dụng cho các gói vật phẩm luôn bán trên WebShop

    - Trong khoảng thời gian: Áp dụng cho các gói vật phẩm chỉ bán theo event
    
    - Tham số mở rộng (tùy chọn, theo thông tin của sản phẩm): tham số được thêm theo các yêu cầu đặc biệt (liên hệ domain: LinhNN6 để trao đổi nếu có yêu cầu)

- Thông tin hiển thị

    ![](media/thongtinhienthi.png)

    - Tên gói: Tên hiển thị của gói vật phẩm

    - Nhóm gói: Chọn Nhóm gói (Tab) cho gói vật phẩm

    - Hình ảnh: Upload hình theo đúng kích thước

    - Mô tả: Nhập vào mô tả gói vật phẩm

- Thông tin thanh toán

    ![](media/thongtinthanhtoan.png)

    - Nhập thông tin về Đơn vị tiền và Giá bán của gói vật phẩm (có thể thêm nhiều giá của gói vật phẩm nếu sử dụng nhiều đơn vị tiền để thanh toán)

    - Giá nhập vào là  giá giao hàng cho đối tác (Tùy thuộc vào chính sách tỉ giá  của VNG)

    - Kênh thanh toán: chi tiết các kênh thanh toán được áp dụng cho gói vật phẩm

2. **Hướng dẫn cấu hình vật phẩm** 

<p style="color:red"><b>Trước khi thao tác nên tham khảo thông tin có sẵn của các gói cũ</b></p>

- Bước 1: Vào tab **Gói được bán** để **Xem** thông tin cơ bản và có thể  chọn **Sửa** để thay đổi thông tin chi tiết các gói vật phẩm hiện có

    - Các thông tin thường thay đổi cần lưu ý: 

        - Trạng thái

        - Thứ tự hiển thị

        - Thời gian bán

        - Tên gói

        - Nhóm gói

        - Hình ảnh

        - Mô tả

        - Giá tiền

- Bước 2:  Chọn **Thêm gói mới** để cấu hình gói vật phẩm mới trên WebShop

- Bước 3: Điền đầy đủ các ô thông tin với đúng thông tin đã khai báo với đối tác và chọn **Xác nhận**

3. **Áp dụng cấu hình (release)**

- Nếu là WebShop tại môi trường Live thì sẽ có thêm phần release và xem được nội dung thay đổi tại trang chủ của WebShop

![](media/live.png)

- Nếu là WebShop tại môi trường Sandbox thì áp dụng luôn tại **2. Hướng dẫn cấu hình gói vật phẩm**

### III. Xem thông tin về các kênh thanh toán của Cửa hàng

- Chọn tab **Kênh thanh toán** trên Danh mục của để xem tất cả các kênh thanh toán hiện có được áp dụng cho WebShop này

![](media/kenhthanhtoan.png)

- Chọn **Xem** để xem chi tiết thông tin của Đơn vị cung cấp kênh thanh toán đó

![](media/thongtinchitietktt.png)

### IV. Theo dõi lịch sử thao tác và phiên bản của Cửa Hàng 

- **Lịch sử cập nhật:** Lịch sử cập nhật sẽ ghi nhận chi tiết những thay đổi trên WebShop

    - Chọn khoảng thời gian muốn xem của **Lịch sử cập nhật** và chọn xem **Chi tiết** 

![](media/lichsu.png)

- Chọn **Xem trước** tại tab **Thông tin chung** để xem chi tiết các thông tin đã được thay đổi 

![](media/xemtruoc.png)


