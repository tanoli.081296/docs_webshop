# HƯỚNG DẪN DỬ DỤNG WEBSHOP REPORT TOOL

## Giới thiệu chung

***WEBSHOP REPORT TOOL là công cụ phục vụ cho việc báo cáo cửa hàng (WebShopReport)***

***Chức năng công cụ ở phiên bản hiện tại gồm có:***

  - Báo cáo bán hàng
    - Báo cáo tổng quan
    - Item Report
    - Payment Channel Report
    - Top Paying User
    - Server Report
    - Compare Shopfront Report 

  - Báo cáo GA (Google Analytics)
    - Time To Complete The Transaction
    - Conversion Rate Detail

## Tính năng

### I. Báo cáo bán hàng

  ![](static/sale-report-overview.png){: .full-width}

### II. Báo cáo GA

#### 1. Chức năng

  ![](static/ga-report-overview.png){: .full-width}
  
#### 2. Cách tính toán thời gian hoàn thành các bước

##### 2.1 Cách tính cho theme newtheme 
  Ví dụ sản phẩm dùng theme newtheme https://pay.zing.vn/wplogin/mobile/jxm
    
  ![](static/ga-report-newtheme.png){: .full-width}

##### 2.2 **Cách tính cho theme primo** 
  Ví dụ sản phẩm dùng theme primo https://pay.zing.vn/product/mua
   
  ![](static/ga-report-primo.png){: .full-width}

##### 2.3 Loại bỏ các giá trị ngoài biên

  Trong quá trình tính có loại bỏ các giá trị time_finish_create_order ngoại biên ngoài giới hạn Lower(L), Upper(U) trong đó L,U được tính như bên dưới:
   
  Ngoài các tham số như giá trị trung bình và trung vị để đo lường khuynh hướng trung tâm hay độ lệch chuẩn để đo lường tính biến thiên, người ta còn dùng các tham số chỉ vị trí của phân bố. Một vài ví dụ về tham số chỉ vị trí như trung vị (median)- điểm giữa của phân bố; min-max, giá trị nhỏ và lớn nhất của phân bố hoặc z-score- đại lượng cho biết một quan sát lệch khỏi giá trị trung bình bao nhiêu lần độ lệch chuẩn.

  Trung vị thực chất là trường hợp đặc biệt của tham số chỉ vị trí có tên gọi là bách phân vị (percentiles). Bách phân vị thứ p (pth percentile) là vị trí có p phần trăm trên tổng số quan sát nhận giá trị nhỏ hơn hoặc bằng giá trị tại điểm đó ( với điều kiện dữ liệu đã được sắp xếp theo thứ tự từ nhỏ đến lớn). Trung vị là giá trị giữa của quan sát, vì vậy ta còn gọi trung vị là bách phân vị thứ 50 (50th percentile), nghĩa là có 50% tổng số quan sát nhận giá trị nhỏ hơn hoặc bằng giá trị tại điểm bách phân vị thứ 50.

  ![](static/percentile.jpg){: .center}

  Một dạng đặc biệt của bách phân vị đó là tứ phân vị (quartiles). Có 3 tứ phân vị:

  - Tứ phân vị thứ nhất (first quartile, Q1) tương ứng với bách phân vị thứ 25 (25th percentile).
  - Tứ phân vị thứ hai (second quartile, Q2, hay median) tương ứng với bách phân vị thứ 50 (50th percentile). 
  - Tứ phân vị thứ ba (third quartile, Q3) tương ứng với bách phân vị thứ 75 (75th percentile).

  ![](static/quartiles.png){: .center}

  Tứ phân vị được xác định như sau:
  
  - Sắp xếp dữ liệu theo thứ tự tăng dần
  - Tìm trung vị, đây là Q2
  - Tìm trung vị của dãy số ở dưới Q2 (lưu ý nếu tổng số quan sát là số lẻ thì không tính trung vị trong khoảng này), đây là Q1
  - Tìm trung vị của dãy số ở trên Q2 (lưu ý nếu tổng số quan sát là số lẻ thì không tính trung vị trong khoảng này), đây là Q3

  Khoảng tứ phân vị (interquartile range, IQR) là khoảng cách giữa Q1 và Q3.
  Công thức tính: IQR= Q3-Q1

  ![](static/iqr.jpg){: .center}

  Một phương pháp xác định giá trị ngoại biên (potential outlier):
  
  Ta gọi:
  
  - L (tức lower) là giá trị thấp của biến, L = Q1 – 1,5*IQR
  - U (tức upper) là giá trị cao của biến, U = Q3 + 1,5*IQR
  - Kết luận: Nếu trong dãy số có số nào thấp hơn L hay cao hơn U thì có thể xem đó là outlier.

  Phương pháp kiểm định outlier trên còn gọi là phương pháp phi tham số, ngoài ra còn có phương pháp dựa vào giả định phân phối chuẩn và phương pháp dựa vào số trung vị. Tuy nhiên phương pháp phi tham số là phương pháp phổ biến và cũng dễ ứng dụng nhất, đặc biệt là trong trường hợp dữ liệu không tuân theo luật phân phối chuẩn. 
    
  Nguồn https://phamtthuytien.blogspot.com/2015/07/tk7-thong-ke-mo-ta-o-luong-tinh-bien.html

